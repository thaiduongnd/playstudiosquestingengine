FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["PlayStudios.QuestingEngine/PlayStudios.QuestingEngine/PlayStudios.QuestingEngine.csproj", "PlayStudios.QuestingEngine/PlayStudios.QuestingEngine/"]
COPY ["PlayStudios.QuestingEngine/PlayStudios.QuestingEngine.Application/PlayStudios.QuestingEngine.Application.csproj", "PlayStudios.QuestingEngine/PlayStudios.QuestingEngine.Application/"]
RUN dotnet restore "PlayStudios.QuestingEngine/PlayStudios.QuestingEngine.Application/PlayStudios.QuestingEngine.Application.csproj"
RUN dotnet restore "PlayStudios.QuestingEngine/PlayStudios.QuestingEngine/PlayStudios.QuestingEngine.csproj"
COPY . .
WORKDIR "/src/PlayStudios.QuestingEngine/PlayStudios.QuestingEngine"
RUN dotnet build "PlayStudios.QuestingEngine.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PlayStudios.QuestingEngine.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PlayStudios.QuestingEngine.dll"]