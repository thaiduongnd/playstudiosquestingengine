# PlayStudiosQuestingEngine


## Getting started

You have 3 ways to run this project

### 1. Run debugging in Visual Studio
- You need Visual Studio 2022, .Net 6 and MongoDb installed in your PC
- Change the connection string in appsetting.json file to your MongoDB connection string
![image.png](./image.png)
- Run VS for Swagger :)

### 2. Build the application with dockerfile and docker-compose
- You don't need Visual Studio, but you still need .Net 6 installed, and docker also.
- Go to the root directory of the repository, you will find the dockerfile and docker-compose.yml file
- Open cmd on this directory, execute 2 commands to build and run the docker container
```
docker-compose build 
docker-compose up
```
- After the container is running, go to http://localhost:8000/swagger to access the swagger for testing

### 3. Just the docker-compose
- You don't even need to clone this repository to run the application as I already publish it to docker hub, you just need to have docker installed
- Download the docker-compose.yml file and put it to any folder
- Open cmd on that folder and run 
```
docker-compose up
``` 
- Wait for docker pull the project and mongo, and then you can access http://localhost:8000/swagger

## The payload for test
- /api/progress
```
{
  "playerId": "Duong",
  "playerLevel": 20,
  "chipAmountBet": 5000000
}
{
  "playerId": "Charles",
  "playerLevel": 50,
  "chipAmountBet": 100000
}
```
- /api/state/{playerId}

- In the data seeding service, I already add 2 playerId that have quest assigned: "Duong" and "Charles"


## Sequence Diagram of a milestone completion:
![image-1.png](./image-1.png)

## The Configuration

### NumberOfMilestonePerQuest:
    Obviously, it describe how many milestone for a quest.
### RateFromBets: 
    A list of rate, base on how many chip is bet
    - FloorAmountBet: If the amount of chip is larger than this, the corresponding rate is applied. That means we will get the Rate that have FloorAmountBet closest and smaller than the bet amount.
    - Rate: The rate 😂

### LevelBonusRates
    Same as RateFomBets, it will have the limit, if player level reach that limit, the rate will be changed
