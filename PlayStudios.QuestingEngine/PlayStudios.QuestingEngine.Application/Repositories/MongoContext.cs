﻿using MongoDB.Driver;

public class MongoContext : IMongoContext
{
    public IMongoDatabase Database { get; }
    public MongoContext(IMongoDbSetting connectionSetting)
    {
        var client = new MongoClient(connectionSetting.ConnectionString);
        Database = client.GetDatabase(connectionSetting.DatabaseName);
    }
}