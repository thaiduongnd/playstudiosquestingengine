﻿using MongoDB.Driver;

public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
{
    protected readonly IMongoDatabase Database;
    protected readonly IMongoCollection<TEntity> DbSet;

    protected BaseRepository(IMongoContext context)
    {
        Database = context.Database;
        DbSet = Database.GetCollection<TEntity>(typeof(TEntity).Name);
    }

    public virtual async Task<TEntity> AddAsync(TEntity entity)
    {
        await DbSet.InsertOneAsync(entity);
        return entity;
    }

    public async Task<IEnumerable<TEntity>> AddManyAsync(IEnumerable<TEntity> entities)
    {
        await DbSet.InsertManyAsync(entities);
        return entities;
    }

    public virtual async Task<TEntity> GetByIdAsync(Guid id)
    {
        var data = await DbSet.Find(FilterId(id)).SingleOrDefaultAsync();
        return data;
    }

    public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
    {
        var all = await DbSet.FindAsync(Builders<TEntity>.Filter.Empty);
        return all.ToList();
    }

    public async virtual Task<TEntity> UpdateAsync(Guid id, TEntity entity)
    {
        await DbSet.ReplaceOneAsync(FilterId(id), entity);
        return entity;
    }

    public async virtual Task<bool> RemoveAsync(Guid id)
    {
        var result = await DbSet.DeleteOneAsync(FilterId(id));
        return result.IsAcknowledged;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
    private static FilterDefinition<TEntity> FilterId(Guid key)
    {
        return Builders<TEntity>.Filter.Eq("Id", key);
    }

}