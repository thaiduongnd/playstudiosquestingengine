﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

public class ConfigurationRepository : BaseRepository<Configuration>, IConfigurationRepository
{
    public ConfigurationRepository(IMongoContext context) : base(context)
    {
    }
}
