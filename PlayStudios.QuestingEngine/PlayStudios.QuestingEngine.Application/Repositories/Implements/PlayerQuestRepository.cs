﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

public class PlayerQuestRepository : BaseRepository<PlayerQuest>, IPlayerQuestRepository
{

    public PlayerQuestRepository(IMongoContext context) : base(context)
    {
    }

    public async Task<PlayerQuest> GetByPlayerIdAsync(string id)
    {
        return (await GetAllAsync()).FirstOrDefault(x => x.PlayerId == id);
    }
}
