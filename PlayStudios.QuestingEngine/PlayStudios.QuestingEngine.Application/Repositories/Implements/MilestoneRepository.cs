﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

public class MilestoneRepository : BaseRepository<Milestone>, IMilestoneRepository
{
    public MilestoneRepository(IMongoContext context) : base(context)
    {
    }
}
