﻿public interface IRepository<TEntity> : IDisposable where TEntity : class
{
    Task<TEntity> AddAsync(TEntity entity);
    Task<IEnumerable<TEntity>> AddManyAsync(IEnumerable<TEntity> entities);
    Task<TEntity> GetByIdAsync(Guid id);
    Task<IEnumerable<TEntity>> GetAllAsync();
    Task<TEntity> UpdateAsync(Guid id, TEntity entity);
    Task<bool> RemoveAsync(Guid id);
}