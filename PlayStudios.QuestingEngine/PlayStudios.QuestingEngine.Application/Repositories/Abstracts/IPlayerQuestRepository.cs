﻿public interface IPlayerQuestRepository : IRepository<PlayerQuest>
{
    Task<PlayerQuest> GetByPlayerIdAsync(string id);
}
