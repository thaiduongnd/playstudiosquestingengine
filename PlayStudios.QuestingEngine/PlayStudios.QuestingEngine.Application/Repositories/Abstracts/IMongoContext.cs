﻿using MongoDB.Driver;

public interface IMongoContext
{
    IMongoDatabase Database { get; }
}