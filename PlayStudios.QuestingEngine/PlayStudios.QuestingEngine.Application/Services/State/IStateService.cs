﻿
public interface IStateService
{
    Task<GetStateResponse> GetPlayerQuestState(string playerId);
}

