﻿public class StateService : IStateService
{
    private readonly IPlayerQuestService _playerQuestService;
    public StateService(IPlayerQuestService playerQuestService)
    {
        _playerQuestService = playerQuestService;
    }

    public async Task<GetStateResponse> GetPlayerQuestState(string playerId)
    {
        var quest = await _playerQuestService.GetPlayerQuestAsync(playerId);
        if (quest == null)
        {
            throw new Exception("Player doesn't have any quest! This probably caused by wrong playerId or data in DB is corrupted!");
        }
        var milestonesCompleted = quest.ActiveQuest.Where(x => x.IsCompleted).ToList();

        var questPercentCompleted = 100 * milestonesCompleted.Count / quest.ActiveQuest.Count();

        return new GetStateResponse()
        {
            LastMilestoneIndexCompleted = milestonesCompleted.Any() ? milestonesCompleted.Max(x => x.Index) : -1,
            TotalQuestPercentCompleted = questPercentCompleted
        };
    }
}

