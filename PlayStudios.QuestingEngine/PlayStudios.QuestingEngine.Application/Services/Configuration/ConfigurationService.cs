﻿public class ConfigurationService : IConfigurationService
{
    private readonly IConfigurationRepository _configurationRepository;
    public ConfigurationService(IConfigurationRepository configurationRepository)
    {
        _configurationRepository = configurationRepository;
    }

    public async Task<LevelBonusRate> GetLevelBonusRateAsync(int level)
    {
        var config = await GetConfigurationAsync();
        var levelBonusRate = config.LevelBonusRates.Where(x => x.FloorLevel <= level).OrderByDescending(x => x.FloorLevel).FirstOrDefault();
        return levelBonusRate;
    }

    public async Task<RateFromBet> GetRateFromBetAsync(double chipAmountBet)
    {
        var config = await GetConfigurationAsync();
        var rateFromBet = config.RateFromBets.Where(x => x.FloorAmountBet <= chipAmountBet).OrderByDescending(x => x.FloorAmountBet).FirstOrDefault();
        return rateFromBet;
    }

    private async Task<Configuration> GetConfigurationAsync()
    {
        var config = (await _configurationRepository.GetAllAsync()).FirstOrDefault();
        if (config == null)
            throw new Exception("Error while getting configuration");
        return config;
    }
}

