﻿public interface IConfigurationService
{
    Task<RateFromBet> GetRateFromBetAsync(double chipAmountBet);
    Task<LevelBonusRate> GetLevelBonusRateAsync (int level);
}

