﻿
public interface IDataSeedingService
{
    Task Seed();
}

