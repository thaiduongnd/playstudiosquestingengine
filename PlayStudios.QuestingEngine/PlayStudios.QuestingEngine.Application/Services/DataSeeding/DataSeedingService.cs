﻿public class DataSeedingService : IDataSeedingService
{
    private readonly IMilestoneRepository _milestoneRepository;
    private readonly IPlayerQuestRepository _playerQuestRepository;
    private readonly IConfigurationRepository _configurationRepository;
    public DataSeedingService(IMilestoneRepository milestoneRepository,
        IPlayerQuestRepository playerQuestRepository,
        IConfigurationRepository configurationRepository)
    {
        _milestoneRepository = milestoneRepository;
        _playerQuestRepository = playerQuestRepository;
        _configurationRepository = configurationRepository;
    }

    public async Task Seed()
    {
        var existedConfiguration = await _configurationRepository.GetAllAsync();
        if (existedConfiguration != null && existedConfiguration.Any())
        {
            return;
        }

        await SeedConfiguration();
        var addedMilestones = await SeedMilestoneData();
        await SeedPlayerQuest(addedMilestones);
    }

    private async Task SeedConfiguration()
    {
        var configuration = new Configuration
        {
            RateFromBets = GetSampleRateFromBet(),
            LevelBonusRates = GetSampleLevelBonusRate(),
            NumberOfMilestonePerQuest = 3
        };
        await _configurationRepository.AddAsync(configuration);
    }
    private async Task<IEnumerable<Milestone>> SeedMilestoneData()
    {
        return await _milestoneRepository.AddManyAsync(GetSampleMilestoneData());
    }
    private async Task SeedPlayerQuest(IEnumerable<Milestone> addedMilestones)
    {
        var numberOfMilestones = (await _configurationRepository.GetAllAsync()).FirstOrDefault()?.NumberOfMilestonePerQuest ?? 3;

        var listPlayerQuest = new List<PlayerQuest>
        {
            new PlayerQuest
            {
                PlayerId = "Duong",
                ActiveQuest = PickSomeMilestonesForQuest(addedMilestones, numberOfMilestones),
            },
            new PlayerQuest
            {
                PlayerId = "Charles",
                ActiveQuest = PickSomeMilestonesForQuest(addedMilestones, numberOfMilestones),
            },
        };
        await _playerQuestRepository.AddManyAsync(listPlayerQuest);
    }


    private IEnumerable<RateFromBet> GetSampleRateFromBet()
    {
        var rateConfigs = new List<RateFromBet>
        {
            new RateFromBet()
            {
                FloorAmountBet = 0,
                Rate = 1
            },
            new RateFromBet()
            {
                FloorAmountBet = 100000,
                Rate = 1.1f
            },
            new RateFromBet()
            {
                FloorAmountBet = 200000,
                Rate = 1.2f
            },
            new RateFromBet()
            {
                FloorAmountBet = 300000,
                Rate = 1.3f
            },
            new RateFromBet()
            {
                FloorAmountBet = 500000,
                Rate = 1.4f
            },
            new RateFromBet()
            {
                FloorAmountBet = 800000,
                Rate = 1.5f
            },
        };
        return rateConfigs;
    }
    private IEnumerable<LevelBonusRate> GetSampleLevelBonusRate()
    {
        var levelBonusRateConfig = new List<LevelBonusRate>
        {
            new LevelBonusRate()
            {
                FloorLevel = 0,
                Rate = 10000
            },
            new LevelBonusRate()
            {
                FloorLevel = 10,
                Rate = 20000
            },
            new LevelBonusRate()
            {
                FloorLevel = 20,
                Rate = 30000
            },
            new LevelBonusRate()
            {
                FloorLevel = 30,
                Rate = 50000
            },
            new LevelBonusRate()
            {
                FloorLevel = 50,
                Rate = 80000
            },
            new LevelBonusRate()
            {
                FloorLevel = 80,
                Rate = 130000
            },
        };
        return levelBonusRateConfig;
    }
    private IEnumerable<Milestone> GetSampleMilestoneData()
    {
        var listMilestone = new List<Milestone>
        {
            new Milestone
            {
                Description = "Earn 1M Quest point",
                QuestPointToComplete = 1000000,
                Award = 1000000
            },
            new Milestone
            {
                Description = "Earn 2M Quest point",
                QuestPointToComplete = 2000000,
                Award = 2000000
            },
            new Milestone
            {
                Description = "Earn 3M Quest point",
                QuestPointToComplete = 3000000,
                Award = 3000000
            },
            new Milestone
            {
                Description = "Earn 5M Quest point",
                QuestPointToComplete = 5000000,
                Award = 5000000
            },
            new Milestone
            {
                Description = "Earn 8M Quest point",
                QuestPointToComplete = 8000000,
                Award = 8000000
            },

        };
        return listMilestone;
    }
    private IEnumerable<QuestMilestone> PickSomeMilestonesForQuest(IEnumerable<Milestone> addedMilestones, int numberOfMilestone)
    {
        var milestones = addedMilestones.OrderBy(x => Guid.NewGuid()).Take(numberOfMilestone);
        return milestones.Select((x, index) => new QuestMilestone
        {
            Index = index,
            IsCompleted = false,
            MilestoneId = x.Id,
            ChipsAwarded = x.Award,
            CurrentQuestPoint = 0,
            QuestPointToComplete = x.QuestPointToComplete,
        });
    }
}

