﻿public class ProgressService : IProgressService
{
    private readonly IPlayerQuestService _playerQuestService;
    private readonly IConfigurationService _configurationService;

    public ProgressService(IPlayerQuestService playerQuestService, IConfigurationService configurationService)
    {
        _playerQuestService = playerQuestService;
        _configurationService = configurationService;
    }
    public async Task<UpdateProgressResponse> UpdateMilestoneProgress(UpdateProgressRequest request)
    {
        var response = new UpdateProgressResponse();

        var quest = await _playerQuestService.GetPlayerQuestAsync(request.PlayerId);
        if (quest == null)
            throw new Exception("Player doesn't have any quest! This probably caused by wrong playerId or data in DB is corrupted!");

        var currentQuestMilestone = quest.ActiveQuest.Where(x => !x.IsCompleted).OrderBy(x => x.Index).FirstOrDefault();
        if (currentQuestMilestone == null)
        {
            return new UpdateProgressResponse()
            {
                QuestPointsEarned = 0,
                TotalQuestPercentCompleted = 100,
                MilestoneCompleted = quest.ActiveQuest,
            };
        }

        var questPointsEarned = await CalculateQuestPointEarned(request);
        response.QuestPointsEarned = questPointsEarned;

        await UpdatePlayerQuestProgress(quest,currentQuestMilestone,questPointsEarned);

        var milestonesCompleted = quest.ActiveQuest.Where(x => x.IsCompleted).ToList();
        response.MilestoneCompleted = milestonesCompleted;
        response.TotalQuestPercentCompleted = 100 * milestonesCompleted.Count / quest.ActiveQuest.Count();

        return response;
    }

    private async Task UpdatePlayerQuestProgress(PlayerQuest quest, QuestMilestone currentQuestMilestone, double questPointsEarned)
    {
        currentQuestMilestone.CurrentQuestPoint += questPointsEarned;

        if (currentQuestMilestone.CurrentQuestPoint >= currentQuestMilestone.QuestPointToComplete)
        {
            currentQuestMilestone.IsCompleted = true;
        }

        await _playerQuestService.UpdatePlayerQuestAsync(quest);
    }
    private async Task<double> CalculateQuestPointEarned(UpdateProgressRequest request)
    {
        var rateFromBet = await _configurationService.GetRateFromBetAsync(request.ChipAmountBet);
        var levelBonusRate = await _configurationService.GetLevelBonusRateAsync(request.PlayerLevel);
        return request.ChipAmountBet * rateFromBet.Rate + request.PlayerLevel * levelBonusRate.Rate;
    }

}

