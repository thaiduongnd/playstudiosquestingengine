﻿
public interface IProgressService
{
    Task<UpdateProgressResponse> UpdateMilestoneProgress(UpdateProgressRequest request);
}

