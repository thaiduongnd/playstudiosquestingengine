﻿public interface IPlayerQuestService
{
    Task<PlayerQuest> GetPlayerQuestAsync(string playerId);
    Task<PlayerQuest> UpdatePlayerQuestAsync(PlayerQuest playerQuest);
}

