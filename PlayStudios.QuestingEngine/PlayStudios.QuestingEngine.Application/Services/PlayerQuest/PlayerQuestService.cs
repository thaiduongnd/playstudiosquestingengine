﻿public class PlayerQuestService : IPlayerQuestService
{
    private readonly IPlayerQuestRepository _playerQuestRepository;

    public PlayerQuestService(IPlayerQuestRepository playerQuestRepository, IMilestoneRepository milestoneRepository, IConfigurationService configurationService)
    {
        _playerQuestRepository = playerQuestRepository;
    }

    public async Task<PlayerQuest> GetPlayerQuestAsync(string playerId)
    {
        return await _playerQuestRepository.GetByPlayerIdAsync(playerId);
    }

    public async Task<PlayerQuest> UpdatePlayerQuestAsync(PlayerQuest playerQuest)
    {
        return await _playerQuestRepository.UpdateAsync(playerQuest.Id, playerQuest);
    }
}

