﻿using MongoDB.Bson.Serialization.Attributes;

public class PlayerQuest
{
    [BsonId]
    public Guid Id { get; set; }
    public string PlayerId { get; set; }
    public IEnumerable<QuestMilestone> ActiveQuest { get; set; }

}
