﻿
using MongoDB.Bson.Serialization.Attributes;

public class Configuration
{
    [BsonId]
    public Guid Id { get; set; }
    public IEnumerable<RateFromBet> RateFromBets { get; set; }
    public IEnumerable<LevelBonusRate> LevelBonusRates { get; set; }
    public int NumberOfMilestonePerQuest { get; set; }
}
