﻿public class QuestMilestone
{
    public int Index { get; set; }
    public Guid MilestoneId { get; set; }
    public bool IsCompleted { get; set; }
    public double ChipsAwarded { get; set; }
    public double QuestPointToComplete { get; set; }
    public double CurrentQuestPoint { get; set; }

}
