﻿using MongoDB.Bson.Serialization.Attributes;

public class Milestone
{
    [BsonId]
    public Guid Id { get; set; }
    public string Description { get; set; }
    public double Award { get; set; }
    public double QuestPointToComplete { get; set; }

}
