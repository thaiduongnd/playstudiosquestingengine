﻿public class UpdateProgressRequest
{
    public string PlayerId { get; set; }
    public int PlayerLevel { get; set; }
    public double ChipAmountBet { get; set; }
}
