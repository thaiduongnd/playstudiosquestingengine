﻿public class UpdateProgressResponse
{
    public double QuestPointsEarned { get; set; }
    public int TotalQuestPercentCompleted { get; set; }
    public IEnumerable<QuestMilestone> MilestoneCompleted { get; set; }
}
