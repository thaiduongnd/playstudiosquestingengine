﻿public class GetStateResponse
{
    public int TotalQuestPercentCompleted { get; set; }
    public int LastMilestoneIndexCompleted { get; set; }
}
