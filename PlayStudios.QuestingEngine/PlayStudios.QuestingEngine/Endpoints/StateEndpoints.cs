﻿public static class StateEndpoints
{
    public static void MapStateEndpoints(this WebApplication app)
    {
        app.MapGet("/api/state/{playerId}", GetState);
    }

    public static async Task<IResult> GetState(this IStateService service, string playerId)
    {
        var state = await service.GetPlayerQuestState(playerId);

        return Results.Ok(state);
    }

}
