﻿public static class ProgressEndpoints
{
    public static void MapProgressEndpoints(this WebApplication app)
    {
        app.MapPost("/api/progress", UpdateProgress);
    }

    public static async Task<IResult> UpdateProgress(this IProgressService service, UpdateProgressRequest request)
    {
        var response = await service.UpdateMilestoneProgress(request);

        return Results.Ok(response);
    }
}
