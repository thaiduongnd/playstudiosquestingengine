using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//application services
builder.Services.Configure<MongoDbSetting>(builder.Configuration.GetSection("MongoDb"));
builder.Services.AddSingleton<IMongoDbSetting>(x => x.GetRequiredService<IOptions<MongoDbSetting>>().Value);
builder.Services.AddScoped<IMongoContext, MongoContext>();

builder.Services.AddScoped<IMilestoneRepository, MilestoneRepository>();
builder.Services.AddScoped<IPlayerQuestRepository, PlayerQuestRepository>();
builder.Services.AddScoped<IConfigurationRepository, ConfigurationRepository>();

builder.Services.AddScoped<IConfigurationService, ConfigurationService>();
builder.Services.AddScoped<IDataSeedingService, DataSeedingService>();
builder.Services.AddScoped<IPlayerQuestService, PlayerQuestService>();
builder.Services.AddScoped<IProgressService, ProgressService>();
builder.Services.AddScoped<IStateService, StateService>();

builder.WebHost.ConfigureKestrel(options => options.Listen(System.Net.IPAddress.Any,80));
builder.WebHost.UseUrls("http://*:80");

var app = builder.Build();

//seeding data
using (var scope = app.Services.CreateScope())
{
    var dataSeedingService = scope.ServiceProvider.GetRequiredService<IDataSeedingService>();
    await dataSeedingService.Seed();
}

app.UseSwagger();
app.UseSwaggerUI();
app.UseExceptionHandler(x => x.Run(async context =>
{
    var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
    var exception = exceptionHandlerPathFeature?.Error;
    await context.Response.WriteAsJsonAsync(new { error = exception?.Message });
}));

//application endpoints
app.MapProgressEndpoints();
app.MapStateEndpoints();

app.Run();

